

<table border=0 cellpadding=10 >

<tr><td width=50 valign=top>
<a href="/drupalstatsuser">
<img src="<?PHP echo drupal_get_path('module', 'drupalstats'); ?>/images/system-users.png" />
</a>
</td><td valign=top> 
<a href="/drupalstatsuser">User Activity Report</a><br />
The number of accounts which logged in within the window of time.

</td></tr>



<tr><td valign=top>
<a href="/drupalstatssession">
<img src="<?PHP echo drupal_get_path('module', 'drupalstats'); ?>/images/office-chart-area-stacked.png" />
</a>
</td><td valign=top> 
<a href="/drupalstatssession">Current Session Activity Report</a><br />
The number of user sessions in the window of time.

</td></tr>

<tr><td valign=top>
<a href="/drupalstatsnodeactivity">
<img src="<?PHP echo drupal_get_path('module', 'drupalstats'); ?>/images/view-time-schedule.png" />
</a>
</td><td valign=top> 
<a href="/drupalstatsnodeactivity">Node Activity Report</a><br />
The number of nodes updated in the window of time.

</td></tr>

<tr><td valign=top>
<a href="/drupalstatsform">
<img src="<?PHP echo drupal_get_path('module', 'drupalstats'); ?>/images/view-calendar-tasks.png" />
</a>
</td><td valign=top>
<a href="/drupalstatsform">Webform Submission Activity Report</a><br />
The number of forms submitted in the window of time.

</td></tr>

</table>
