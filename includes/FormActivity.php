

    <style>
         
        .label {
            color: #666;
            margin-top: 0.5em;
            padding: 0.1em;
            font-family: 'verdana', 'helvetica', sans serif;
        }        
        
        .timeplot-grid-label {
            color: #333;
            font-family: 'verdana', 'helvetica', sans serif;
            font-size: 9px !important;
        }
        
        .sources {
            font-size: 90%;
        }
    </style>
    
    

<script>SimileAjax.History.enabled = false;</script>

<script type="text/javascript">
// The following two line substitude the body onload and onresize described in Step 3
window.onload = function() { onLoad();}
window.onresize = onResize();

        var timeplot2;

        var color1 = new Timeplot.Color('#FF0000');
        var color2 = new Timeplot.Color('#FFB43D');
        var color3 = new Timeplot.Color('#EB800F');
	var color4 = new Timeplot.Color('#0000FF');
        var color5 = new Timeplot.Color('#0000A0');

        var gridColor  = new Timeplot.Color('#333333');
        
        function onLoad() {

            var timeGeometry = new Timeplot.DefaultTimeGeometry({
                gridColor: gridColor
            });


            var geometry2 = new Timeplot.DefaultValueGeometry({
                min: 0,
		max:10
            });
            

            var eventSource2 = new Timeplot.DefaultEventSource();
            var dataSource2 = new Timeplot.ColumnSource(eventSource2,1);

            

	 var plotInfo2 = [
                Timeplot.createPlotInfo({
                    id: "The number of forms submitted in the window of time.",
                    dataSource: dataSource2,
                    timeGeometry: timeGeometry,
                    valueGeometry: geometry2,
                    lineColor: color2,
                        fillColor: color3,
                    showValues: true
                })
            ];
            
            timeplot2 = Timeplot.create(document.getElementById("timeplot2"), plotInfo2);
            timeplot2.loadText("drupalstatsformdata", ",", eventSource2);


        


        }


        var resizeTimerID = null;
        function onResize() {
            if (resizeTimerID == null) {
                resizeTimerID = window.setTimeout(function() {
                    resizeTimerID = null;
                    if (timeplot2) timeplot2.repaint();
                }, 0);
            }
        }
    </script>
  
 

  
<div class="label"><span style="color: #66463E;">The number of forms submitted in the window of time.</span> </div>

        <div id="timeplot2" style="height: 300px; width: 900px" class="timeplot"></div>
    
 
<?PHP 
require("footer.php");
?>
    


